# Desktop-Computer-Drive-Bay-Grip

This is a 3D-printable grip to insert into a desktop computer's 5.25 inch drive bay.

![](https://raw.githubusercontent.com/Bengt/Desktop-Computer-Drive-Bay-Grip/master/images/7a2979d10f370bc49e799cca4c77f210_preview_featured_cut.jpg)
![](https://raw.githubusercontent.com/Bengt/Desktop-Computer-Drive-Bay-Grip/master/images/459516b8936e5a27f0aff2a1dd234d45_preview_featured_cut.jpg)

## Rationale

With optical drives going out of fashion, 5.25 Inch bays are no longer of much use. Leaving them empty makes for an handy carrying grip. However, such an opening is a gateway for dust entering the computer case. Moreover, sharp edges inside the case may endanger the health of the carrying person. This grip closes inserts into an unused  5.25 inch bay, providing a safe grip and dust protection.

## Customisation

Parameters for material strenght and grip depth can be customised in the scad file. I also enabled the Customizer, but that does not seem to work. Any help with fixing that would be most welcome.

## Printing

There is a step around the front, so the best way of printing is probably to turn the grip on its front and print it from front to rear upwards.

## Installation

It is most ergonomical to install the grip into the top most drive bay. That way, the carrying hand to wrap around the edge to the top of the case. The other hand can grip at the rear of the case beneath the power supply unit or the opposing edge of the case. Installing the grip into the top of the case also enables the user to line the case in order to support the grip for increased stability.

## Status

I made some changes after publishing this file. Critically, the tap holes had the wrong size. Please make sure you have the latest version.

## Thingiverse

This thing is also available on [Thingiverse](http://www.thingiverse.com/thing:1412224/).
