$fn=128;

// 5.25" dimensions
// compare with Specification for Form Factor of 5.25" CD Drives at
// ftp://ftp.seagate.com/sff/SFF-8551.PDF
A17 = 5.00;
A1 = 41.53;
A2 = 42.30;
A3 = 148.00;
A4 = 202.80;
A5 = 146.05;
A6 = 139.70;
A7 = 3.18;
A8 = 79.25;
A9 = 47.40;
A10 = 47.40;
A11 = 79.25;
A13 = 10.00;
A14 = 21.84;
A16 = 6.50;
A17 = 5.00;

A4short = 2*A9 + (A1 / 3.14159);

// Metric screw Metric ISO 724 M3 tap drill radius
// Compare to ISO metric screw thread at
// http://www.engineeringtoolbox.com/metric-threads-d_777.html
m3r = 2.5 / 2; 

// material strength
// customise to the capabilities of your 3D printer
strength = 2;

// length of the pillars' edges that the front connects to 
pillar = A9 + 31;

// Compensate 16 bit z-buffer
zbuf = 0.01;

// constrct the front
hollow_front();

module hollow_front() {
    difference (){
        front();
        front_cutout();
    }
}

module front() {
    color("LightBlue")
    // compensate overhang
    translate([- (A3 - A5) / 2, 0, (A1 - A2) / 2])
    translate([0, -A17, 0])
    cube([A3, A17, A2]);
}


module front_cutout() {
    // compensate front thickness
    translate([0, -A17, 0])
    // fix zbuffer
    translate([0, -zbuf, 0])
    // indent
    translate([strength, 0, strength])
    cube([A5 - 2*strength, A17 + 2*zbuf, A1 - 2*strength]);
}

// Construct the corpus
corpus();

module corpus () {
    difference() {
        corpus_shell();
        m3screws();
    }
}

module corpus_shell() {
    difference () {
        roundedcube(A5, A4short, A1, A1/2);
        corpus_cutout();
    }
}

module corpus_cutout() {
    translate([strength, -zbuf, strength])
    roundedcube(xdim=A5 - 2*strength,
                ydim=A4short - strength + zbuf,
                zdim=A1 - 2*strength,
                rdim=A1/2 - strength);
}

module roundedcube(xdim, ydim, zdim, rdim){
    hull(){
        // square edges at the front
        translate([0, 0, 0])cube([pillar, pillar, zdim]);
        translate([xdim-pillar, 0, 0])cube([pillar, pillar, zdim]);
        
        // rounded edges at the rear top left
        translate([rdim, ydim-rdim/2, zdim/2])
        rotate([-90, 0, 0])
        cylinder(h=rdim, r1=rdim, r2=0, center=true);

        // rounded edges at the rear top right
        translate([xdim-rdim, ydim-rdim/2, zdim/2])
        rotate([-90, 0, 0])
        cylinder(h=rdim, r1=rdim, r2=0, center=true);
    }
}

module m3screws(){
    h = strength + 2 * zbuf;
    // front left bottom
    rotate([90, 0, 90])
    translate([A9, A13, h - h/2 - zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);

    // front left top
    rotate([90, 0, 90])
    translate([A9, A14, h - h/2 - zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);

    // rear left bottom
    rotate([90, 0,90])
    translate([A8 + A9, A13, h - h/2 - zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);
    
    // rear left top
    rotate([90, 0, 90])
    translate([A8 + A9, A14, h - h/2 - zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);
    
    // -----------------------

    // front right bottom
    rotate([90, 0,90])
    translate([A9, A13, A5 - h / 2 + zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);

    // front right top
    rotate([90, 0,90])
    translate([A9, A14, A5 - h / 2 + zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);

    // rear right bottom
    rotate([90, 0,90])
    translate([A8 + A9, A13, A5 - h / 2 + zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);
    
    // rear right top
    rotate([90, 0, 90])
    translate([A8 + A9, A14, A5 - h / 2 + zbuf])
    color("white")
    cylinder(h=h, r=1.5, center=true, $fn=100);
}
